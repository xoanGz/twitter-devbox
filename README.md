# Docker Composer for Kafka
This docker composer creates a kafka queue with the topic 'twitter'.
Creates a producer that publish into the topic 'twitter'
Creates a streaming that reads from twitter and create the twitter-sentiment topic and a rest api.
Creates a consumer socket that reads from the topic 'twitter-sentiment'

## Prequisites
### Create image dataspartan/pythonserver (https://bitbucket.org/xoanGz/mock-classify-tweet)
### Create image dataspartan/twitter-producer (https://bitbucket.org/xoanGz/twitter-kafka)
### Create image dataspartan/twitter-streaming (https://bitbucket.org/xoanGz/twitter-streaming)
### Create image dataspartan/twitter-socket-consumer (https://bitbucket.org/xoanGz/twitter-nodejs)
### Create image dataspartan/twitter-angular (https://bitbucket.org/xoanGz/twitter-angular)
### Create folder dependencies
```
# Create dirs for Kafka / ZK data
$ sudo mkdir -p /dockerdata/zookeeper_1/zk-data
$ sudo mkdir -p /dockerdata/zookeeper_1/zk-txn-logs
$ sudo mkdir -p /dockerdata/kafka_1/kakfa-data
```
## Run the docker composer

```
# docker-compose up
...
```

## Stop the docker composer

```
# docker-compose down
...
```

```
Open http://localhost/ for go to the webpage
```